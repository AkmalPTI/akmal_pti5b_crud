<!DOCTYPE html>
<html>
    <head>
        <title>Admin</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://kit.fontawesome.com/fd8370ec87.js" crossorigin="anonymous"></script>
    </head>
<body>
    <div id="navbar" class="mb-4">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Sistem Informasi Pegawai</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a href="/" class="nav-link"> <i
                                class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="mb-4 pl-3">
        <h2>Tambah Mahasiswa</h2>
        <br>
        <form action="/store" method="post">
            {{ csrf_field() }}
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" name="nama" required class="form-control">
                </div>
                <div class="form-group">
                    <label for="">NIM</label>
                    <input type="text" name="jabatan" required class="form-control">
        
                <div class="form-group">
                    <label for="">Kelas</label>
                    <input type="number" name="umur" required class="form-control">
        
                </div>
                <div class="form-group">
                    <label for="">Prodi</label>
                    <textarea type="text" name="alamat" required class="form-control"></textarea>
        
                </div>
                <div class="row"><div class="form-group pl-3">
                    <a href="/" class="btn btn-primary">Batal</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div></div>
            </div>
        </form>
    </div>
	
		


</body>
</html>