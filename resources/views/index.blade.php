<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/fd8370ec87.js" crossorigin="anonymous"></script>
</head>
<body>
    <div id="navbar" class="mb-4">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Sistem Informasi Pegawai</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a href="/" class="nav-link"> <i
                                class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

	<div class="row mb-4 pl-4">
        <div class="pull-left ">
            <h4>Daftar Mahasiswa</h4>
        </div>
        <div class="pull-right pl-3 mb-3">
            <a href="/tambah">
                <button class="btn btn-primary">Tambah Data Pegawai</button>
            </a>
        </div>

        <table class="table ">
            <thead>
                <tr>
                    <td>
                        Id
                    </td>
                    <td>Nama</td>
                    <td>NIM</td>
                    <td>Kelas</td>
                    <th>Prodi</th>
                    <td>Aksi</td>
                </tr>
            </thead>
            <tbody>
                @foreach($pegawai as $p)
                <tr>
                    <td>{{ $p->pegawai_id }}</td>
                    <td>{{ $p->pegawai_nama }}</td>
			        <td>{{ $p->pegawai_jabatan }}</td>
			        <td>{{ $p->pegawai_umur }}</td>
			        <td>{{ $p->pegawai_alamat }}</td>
			        <td>
				        <a href="/edit/{{ $p->pegawai_id }}">Edit</a>
				        |
				        <a href="/hapus/{{ $p->pegawai_id }}">Hapus</a>
			        </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>